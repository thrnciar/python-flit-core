# Tests are disabled by default, unless on EPEL.
# We don't have testpath in RHEL 9.
%bcond tests %{defined epel}

Name:           python-flit-core
Version:        3.9.0
Release:        5%{?dist}
Summary:        PEP 517 build backend for packages using Flit

# flit-core is BSD-3-Clause
# flit_core/versionno.py contains a regex that is from packaging, BSD-2-Clause
License:        BSD

URL:            https://flit.pypa.io/
Source:         %{pypi_source flit_core}

BuildArch:      noarch
BuildRequires:  python3-devel

%if %{with tests}
BuildRequires:  python3-pytest
# Test deps that require flit-core to build:
BuildRequires:  python3-testpath
%endif

%global _description %{expand:
This provides a PEP 517 build backend for packages using Flit.
The only public interface is the API specified by PEP 517,
at flit_core.buildapi.}

%description %_description


%package -n python3-flit-core
Summary:        %{summary}
Conflicts:      python3-flit < 2.1.0-2

%description -n python3-flit-core %_description


%prep
%autosetup -p1 -n flit_core-%{version}
# Add tomli to build-system.requires and project.dependencies to account for
# our tomli unbundling.
# tomllib is a part of standard library since Python 3.11, so we only need this
# for python_version<'3.11'.
sed -i 's/^\(dependencies\|requires\) = \[\]/\1 = ["tomli; python_version<'\'3.11\''"]/' pyproject.toml

# Remove vendored tomli that flit_core includes to solve the circular dependency on older Pythons
# (flit_core requires tomli, but flit_core is needed to build tomli).
rm -rf flit_core/vendor


%generate_buildrequires
%pyproject_buildrequires


%build
%pyproject_wheel


%install
%pyproject_install

# don't ship tests in flit_core package
# if upstream decides to change the installation, it can be removed:
# https://github.com/takluyver/flit/issues/403
rm -r %{buildroot}%{python3_sitelib}/flit_core/tests/

%check
%py3_check_import flit_core flit_core.buildapi
%if %{with tests}
%pytest
%endif


%files -n python3-flit-core
%license LICENSE
%doc README.rst
%{python3_sitelib}/flit_core-*.dist-info/
%{python3_sitelib}/flit_core/


%changelog
* Mon Oct 16 2023 Miro Hrončok <mhroncok@redhat.com> - 3.9.0-5
- Initial package for RHEL 9
- Resolves: RHEL-12338
- Fedora+EPEL contributions by:
      Charalampos Stratakis <cstratak@redhat.com>
      Igor Gnatenko <ignatenkobrain@fedoraproject.org>
      Karolina Surma <ksurma@redhat.com>
      Lumir Balhar <lbalhar@redhat.com>
      Maxwell G <gotmax@e.email>
      Michel Alexandre Salim <salimma@fedoraproject.org>
      Miro Hrončok <miro@hroncok.cz>
      Mukundan Ragavan <nonamedotc@fedoraproject.org>
      Tomáš Hrnčiar <thrnciar@redhat.com>
      Yaakov Selkowitz <yselkowi@redhat.com>
